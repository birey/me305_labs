##@file      Lab04_DataTask.py
#@brief      This code creates a data collection FSM.
#@details    This file collects time and encoder position data.  The data is 
#            then sent back to the PC
#@author     Brennen Irey
#@date       November 4, 2020

import utime
import pyb
#import UART

## Data collection task
#
class DataTask:
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1 - Waiting for command
    S1_WAIT_FOR_CMD     = 1
    
    ## Constant defining State 2 - Data collection
    S2_DATA_COLLECT     = 2
    
    ## Constant defining State 3 - Data sending
    S3_DATA_SEND        = 3
    
    ## Constructs a data collection task
    #
    def __init__(self, taskNum, interval, encoder, port):
        
        ## The number of the task
        self.taskNum = taskNum
        
        ## The interval between tasks in microseconds
        self.interval = interval
        
        ## The UART port being used
        self.myuart = pyb.UART(port)
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## A counter recording how many times the task has been run
        self.runs = 0
        
        ## The microsecond timestamp for the first run
        self.start_time = utime.ticks_us()
        
        ## The microsecond timestamp for the next iteration
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The microsecond timestamp for the current time
        self.curr_time = 0
        
        ## The microsecond timestamp for time elapsed
        self.time_elapsed = utime.ticks_diff(self.curr_time, self.start_time)
        
        ## Creates a copy of the EncoderDriver object
        self.Encoder = encoder
        
        ## Creates a list for the data strings
        self.data = []
        
    ## Runs the task once
    def run(self):
        
         ## The microsecond timestamp for the current iteration
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.Encoder.update()
                if(self.myuart.any() == 71 or self.myuart.any() == 103):
                    self.transitionTo(self.S2_DATA_COLLECT)
                else:
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                
            elif(self.state == self.S2_DATA_COLLECT):
                self.Encoder.update()
                self.data.append('{:},{:} \n'.format(utime.ticks_diff(self.curr_time, self.next_time), self.Encoder.get_position()))
                if(self.myuart.any() == 83 or self.myuart.any() == 115):
                    self.transitionTo(self.S3_DATA_SEND)
                elif(self.time_elapsed >= 10000000):
                    self.transitionTo(self.S3_DATA_SEND)
                else:
                    self.transitionTo(self.S2_DATA_COLLECT)
            
            elif(self.state == self.S3_DATA_SEND):
                self.Encoder.update()
                self.data.append('end')
                for n in self.data:
                    i = 0
                    #UART.write(str(self.data[n])).encode('ascii')
                    print(str(self.data[n]))
                    i += 1
                self.data = []
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                
            else:
                pass
            
            self.runs += 1
            
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
            self.time_elapsed = utime.ticks_diff(self.curr_time, self.start_time)
            
                
                
     ## Updates state    
    def transitionTo(self, newState):
        self.state = newState
    
