##@file      Lab05_LEDTask.py
#@brief      A file that generates a LED Task.
#@details    This file interacts with a UI in order to set the frequency at 
#            which an LED blinks.
#@author     Brennen Irey
#@date       November 12, 2020

import pyb
import utime
import shares


## LED task
#
#@details This object creates an LED Task which receives commands from the UI.
#
#@image html Lab05_LED_Task_FSM.png width = 1200px
class LEDTask:
    
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1 - Waiting for user input
    S1_WAIT_FOR_CMD     = 1

    ## Constructs a LED task
    #@param taskNum A number to identify the task
    #@param interval The time between tasks in microseconds
    #@param dbg Boolean indicating whether debug statements will print    
    def __init__(self, taskNum, interval, dbg):
        
        ## The number of the task
        self.taskNum    = taskNum
        
        ## The interval between tasks in microseconds
        self.interval   = interval
        
        ## Debug status
        self.dbg        = dbg

        ## The state to run on the next iteration of the task
        self.state      = self.S0_INIT
        
        ## Initializes the pin controlling the User LED
        self.pinA5      = pyb.Pin(pyb.Pin.cpu.A5)
        
        ## Initializes Timer 2
        self.tim2       = pyb.Timer(2, freq = 1)
        
        ## Sets Timer 2 Channel 1 to use PWM
        self.t2ch1      = self.tim2.channel(1, pyb.Timer.PWM, pin = self.pinA5)
        
        ## Sets the frequency the LED will blink at
        self.freq       = 0
       
        ## The state to run on the next iteration of the task
        self.state      = self.S0_INIT
        
        ## A counter recording how many times the task has been run
        self.runs       = 0
        
        ## The microsecond timestamp for the first run
        self.start_time = utime.ticks_us()
        
        ## The microsecond timestamp for the next iteration
        self.next_time  = utime.ticks_add(self.start_time, self.interval)        
        
        
    ## Runs the task once    
    def run(self):
       
       ## The microsecond timestamp for the current iteration
       self.curr_time = utime.ticks_us()
       if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
           
           if(self.state == self.S0_INIT):
               self.printTrace()
               # Run State 0 Code
               self.transitionTo(self.S1_WAIT_FOR_CMD)
               
           elif(self.state == self.S1_WAIT_FOR_CMD):
               self.printTrace()
               # Run State 1 Code
               if shares.cmd:
                   self.freq = shares.cmd
                   self.execute((self.freq))
                   shares.cmd = None
                   
                   self.t2ch1.pulse_width_percent(50)
           else:
               pass
           self.runs += 1
           
           # The timestamp in microseconds for the start of the next task
           self.next_time = utime.ticks_add(self.next_time, self.interval)
               
            
            
    ## Updates state        
    def transitionTo(self, newState):
        
        self.state = newState
        
        
    ## Prints a debug statement if the debug variable is True    
    def printTrace(self):
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
    
    ## Executes the command received from the serial port 
    def execute(self, cmd):
        
        self.tim2 = pyb.Timer(2, freq = cmd)
        
        