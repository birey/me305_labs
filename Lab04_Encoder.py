##@file      Lab04_Encoder.py
#@brief      This code creates and encoder driver object.  The encoder driver 
#            configures the timer, pins, period, and prescaler.
#@author     Brennen Irey
#@date       November 4, 2020



import pyb

## Encoder driver object
#
#@details This object sets up a timer and calculates position and change of position 
#of an encoder.
#
#@image html EncoderDriver_Class_Diagram_Lab03.png width = 1200px
class EncoderDriver:
    
    ## Constructs an EncoderDriver object
    def __init__(self, pin1, pin2, timer, period, prescaler):
        
        ## The location of Pin 1
        self.pin1 = pyb.Pin(pin1)
        
        ## The location of Pin 2
        self.pin2 = pyb.Pin(pin2)
        
        ## The number of the timer to be used
        self.timer = timer
        
        ## The period of the timer
        self.period = period
        
        ## The prescaler for the timer
        self.prescaler = prescaler
        
        ## Identifies timer
        self.tim = pyb.Timer(self.timer)
        
        ## Initializes timer with prescaler and period values
        self.tim.init(prescaler = self.prescaler, period = self.period)
        
        ## Initializes timer channel 1
        self.tim.channel(1, pin = self.pin1, mode = pyb.Timer.ENC_AB)
        
        ## Initializes timer channel 2
        self.tim.channel(2, pin = self.pin2, mode = pyb.Timer.ENC_AB)
        
        self.tim.counter() 
        ## Most recent position update
        self.pos1 = 0
        
        ## Second most recent position update
        self.pos2 = 0
        
        ## Difference between two most recent position updates
        self.delta = 0
        
        ## Total movement
        self.position = 0
    
    ## Updates the encoder position 
    def update(self):
        self.tim.counter()
        #print('C{:}'.format(self.tim.counter()))
        self.pos2 = self.pos1
        self.pos1 = self.tim.counter()
        self.delta = self.pos1 - self.pos2
        self.position += self.delta
        #print('pos1{:}'.format(self.pos1))
        #print('delta{:}'.format(self.delta))
    
    ## Returns the current total position, accounts for timer over and underflow    
    def get_position(self):
        return self.position
        
    ## Sets the position to a new value.    
    def set_position(self, new_pos):
        self.pos1 = 0
        self.pos2 = 0
        self.position = new_pos
    
    ## Returns the change in position between the two most recent position updates
    def get_delta(self):
        return self.delta
        
