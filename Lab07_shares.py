'''
@file shares.py
@brief A container for all the inter-task variables
@author Charlie Refvem
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

## The kp value sent from the frontend
cmd     = None

## the response when data collection is done
resp    = None

## The omega array
omega_arr = None

## The time array
time_arr = None

## The omega_ref array
omega_ref = None

## The theta_ref array
theta_ref = None