##@file      Lab07_MotorDriver.py
#@brief      This code creates a motor driver object.
#@details    This file contains the MotorDriver class.  The motor driver configures the timer and pins that the motor will use.  
#@author     Brennen Irey
#@date       November 12, 2020

import pyb

## Motor driver object
#
# @details This object designates which pins and timers will be used to control a 
# motor.  The motor can be enabled/disalbed, and the speed can be controlled.
class MotorDriver:
    
    ## Constructs a MotorDriver object
    # @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
    # @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
    # @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
    # @param timer A pyb.Timer object to use for PWM generation on
    # IN1_pin and IN2_pin.
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, channel1, channel2):
       
        print ('Creating a motor driver')
        
        ## The location of the nSLEEP pin
        self.nSLEEP_pin = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        
        ## Turns off the motor
        self.nSLEEP_pin.low()
        
        ## The location of INPUT 1 pin
        self.IN1_pin = pyb.Pin(IN1_pin)
        
        ## The location of INPUT 2 pin
        self.IN2_pin = pyb.Pin(IN2_pin)
        
        ## Sets up the timer
        self.tim = pyb.Timer(timer, freq = 20000)
        
        ## Sets up the first channel in PWM mode
        self.tch1 = self.tim.channel(channel1, pyb.Timer.PWM, pin = self.IN1_pin)
        
        ## Sets up the second channel in PWM mode
        self.tch2 = self.tim.channel(channel2, pyb.Timer.PWM, pin = self.IN2_pin)
        
    ## Enables motors
    def enable (self):
        print ('Enabling Motor')
        self.nSLEEP_pin.high()
    
    ## Disables motors
    def disable (self):
        print ('Disabling Motor')
        self.nSLEEP_pin.low()
    
    ## This method sets the duty cycle to be sent
    # to the motor to the given level. Positive values
    # cause effort in one direction, negative values
    # in the opposite direction.
    # @param duty A signed integer holding the duty
    # cycle of the PWM signal sent to the moto
    def set_duty (self, duty):
        # Checks if duty is positive and sets 'reverse' channel to 0 PWM percent
        if duty >= 0:
            self.tch2.pulse_width_percent(0)
            self.tch1.pulse_width_percent(duty)
        else:
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(abs(duty))

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = None;
    pin_IN1 = None;
    pin_IN2 = None;
    
    # Create the timer object used for PWM generation
    tim = None;
    
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
    
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 10 percent
    moe.set_duty(10)