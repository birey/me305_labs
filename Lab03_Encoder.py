##@file      Lab03_Encoder.py
#@brief      This code creates and encoder driver object and an encoder task object.
#@details    This file contains both the EncoderDriver class and the EncoderTask class.
#            The encoder driver configures the timer, pins, period, and prescaler.
#            The encoder task regularly updates the position of the encoder.
#@author     Brennen Irey
#@date       October 21, 2020

import shares
import utime
import pyb

## Encoder driver object
#
#@details This object sets up a timer and calculates position and change of position 
#of an encoder.
#
#@image html Lab03_EncoderDriver_Class_Diagram.png width = 1200px
class EncoderDriver:
    
    ## Constructs an EncoderDriver object
    def __init__(self, pin1, pin2, timer, period, prescaler):
        
        ## The location of Pin 1
        self.pin1 = pyb.Pin(pin1)
        
        ## The location of Pin 2
        self.pin2 = pyb.Pin(pin2)
        
        ## The number of the timer to be used
        self.timer = timer
        
        ## The period of the timer
        self.period = period
        
        ## The prescaler for the timer
        self.prescaler = prescaler
        
        ## Identifies timer
        self.tim = pyb.Timer(self.timer)
        
        ## Initializes timer with prescaler and period values
        self.tim.init(prescaler = self.prescaler, period = self.period)
        
        ## Initializes timer channel 1
        self.tim.channel(1, pin = self.pin1, mode = pyb.Timer.ENC_AB)
        
        ## Initializes timer channel 2
        self.tim.channel(2, pin = self.pin2, mode = pyb.Timer.ENC_AB)
        
        self.tim.counter() 
        ## Most recent position update
        self.pos1 = 0
        
        ## Second most recent position update
        self.pos2 = 0
        
        ## Difference between two most recent position updates
        self.delta = 0
        
        ## Total movement
        self.position = 0
    
    ## Updates the encoder position 
    def update(self):
        self.tim.counter()
        #print('C{:}'.format(self.tim.counter()))
        self.pos2 = self.pos1
        self.pos1 = self.tim.counter()
        self.delta = self.pos1 - self.pos2
        self.position += self.delta
        #print('pos1{:}'.format(self.pos1))
        #print('delta{:}'.format(self.delta))
    
    ## Returns the current total position, accounts for timer over and underflow    
    def get_position(self):
        return self.position
        
    ## Sets the position to a new value.    
    def set_position(self, new_pos):
        self.pos1 = 0
        self.pos2 = 0
        self.position = new_pos
    
    ## Returns the change in position between the two most recent position updates
    def get_delta(self):
        return self.delta
        
## Encoder task
#
#@details This object interacts with and encoder driver to determine and set the
#position of the encoder.  It also returns values and executes commands that
#the user inputs.
#
#@image html Lab03_Encoder_Task_FSM.png width = 1200px       
class EncoderTask:
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1 - Waiting for user command
    S1_WAIT_FOR_CMD     = 1
    
    ## Constructs an encoder task
    #@param taskNum A number to identify the task
    #@param interval The time between tasks in microseconds
    #@param encoder An EncoderDriver object
    #@param dbg Boolean indicating whether debug statements will print
    def __init__(self, taskNum, interval, encoder, dbg):
        
        ## The number of the task
        self.taskNum = taskNum
        
        ## The interval between tasks in microseconds
        self.interval = interval
        
        ## Debug status
        self.dbg = dbg
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## A counter recording how many times the task has been run
        self.runs = 0
        
        ## The microsecond timestamp for the first run
        self.start_time = utime.ticks_us()
        
        ## The microsecond timestamp for the next iteration
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Creates a copy of the EncoderDriver object
        self.Encoder = encoder
        
        if self.dbg:
            print('Created encoder task')
    
    ## Runs the task once
    def run(self):
        
        ## The microsecond timestamp for the current iteration
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printTrace()
                # Run State 1 Code
                self.Encoder.update()
                if shares.cmd:
                    shares.resp = self.execute(shares.cmd)
                    shares.cmd = None
            else:
                pass
            self.runs += 1
            
            # The timestamp in microseconds for the start of the next task
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    ## Updates state    
    def transitionTo(self, newState):
        self.state = newState
    
    ## Prints a debug statement if the debug variable is True    
    def printTrace(self):
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
    
    ## Executes the command received from the serial port    
    def execute(self, cmd):
        if cmd == 90 or cmd == 122:
            self.Encoder.set_position(0)
            return 0
        
        elif cmd == 80 or cmd == 112:
            return self.Encoder.get_position()
            
        elif cmd == 68 or cmd == 100:
            return self.Encoder.get_delta()
            
            
       
            
            