##@file     Lab03_main.py

#import shares
from Lab06_Controller import ControllerTask
from Lab06_Encoder import EncoderDriver
from Lab06_MotorDriver import MotorDriver 
from Lab06_ClosedLoop import ClosedLoop 
from Lab06_UserTask import UserTask

## Creates an encoder object
encoder = EncoderDriver('PB6', 'PB7', 4, 0xFFFF, 0)

## Creates an motor object
motor = MotorDriver('PA15', 'PB4', 'PB5', 3, 1, 2)

## Creates an closedloop object
closedloop = ClosedLoop(6000, 0)

## Creates a controller task
task0 = ControllerTask(0, 40000, encoder, motor, closedloop, False)

## Creates a backend UI Task
task1 = UserTask(1, 1000, False)

## Runs tasks sequentially
tasklist = [task0, task1]

# Runs tasks continuously
while True:
    for task in tasklist:
        task.run()