##@file      Lab07_Frontend.py
#@brief      This code creates a frontend UI script.
#@details    This File sends K_p values to the backend and receives collected
#            data from the backend
#@author     Brennen Irey
#@date       December 3, 2020

import serial
import matplotlib.pyplot as plt
import time

ser = serial.Serial(port = 'COM5', baudrate = 115273, timeout = 1)
omega               = []    # Initializes omega array
times               = []    # Initializes time array
omega_ref           = []    # Initializes the reference velocity array
theta_ref           = []    # Initializes the reference position array
time_processed      = []    # Initializes the processed time array
omega_ref_processed = []    # Initializes the processed velocity array
theta_ref_processed = []    # Initializes the processed position array

ref = open('reference.csv');

while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    else:
        (t,v,x) = line.strip().split(',');
        times.append(float(t))
        omega_ref.append(float(v))
        theta_ref.append(float(x))
        
ref.close()

# The following for loop processes the data into a smaller set of data
ind = 0
while ind <= len(times):
    time_processed.append(times[ind])
    omega_ref_processed.append(omega_ref[ind])
    theta_ref_processed.append(theta_ref[ind])
    ind += 20
    

#def FrontEnd():
    
kp = int(input('Please enter a K_p value: '))
print('Collecting data, please wait.')
ser.write(str(kp).encode('ascii'))

time.sleep(1)

ind = 0
for n in time_processed:
    ser.write('{:},{:},{:}'.format(time_processed[ind],omega_ref_processed[ind],theta_ref_processed[ind]).encode('ascii'))
    ind += 1

time.sleep(12)
while True:
    if ser.in_waiting != 0:
        omega.append(ser.readline())
       
    else:
        break
plt.plot(time_processed, omega)
plt.ylabel('Motor Speed, [RPM]')
plt.xlabel('Time, [s]')
plt.show()
#else:
   # print('That is not a valid value.')
    #return

ser.close()
    



