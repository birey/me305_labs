##@file      Lab05_UITask.py
#@brief      This code creates a user interface.
#@details    This file allows users to change the rate at which an LED blinks.
#@author     Brennen Irey
#@date       November 12, 2020

import shares
import utime
import pyb 

## User interface task
#
#@details This object communicates with an LED through the serial 
#UART3 port.  The serial port waits for a numerical input and adjusts the 
#blinking frequency of the LED
#
#@image html Lab05_UI_Task_FSM.png width = 1200px
class UITask:
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1 - Waiting for user input
    S1_WAIT_FOR_CMD        = 1
    
    ## Constructs a user interface task
    #@param taskNum A number to identify the task
    #@param interval The time between tasks in microseconds
    #@param dbg Boolean indicating whether debug statements will print
    def __init__(self, taskNum, interval, dbg):
        
        ## The number of the task
        self.taskNum = taskNum
        
        ## The interval between tasks in microseconds
        self.interval = int(interval)
        
        ## Debug status
        self.dbg = dbg
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## A counter recording how many times the task has been run
        self.runs = 0
        
        ## The microsecond timestamp for the first run
        self.start_time = utime.ticks_us()
        
        ## The microsecond timestamp for the next iteration
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The serial port being addressed
        self.ser = pyb.UART(3, 9600)
        
    
        if self.dbg:
            print('Created user interface task')
    
    ## Runs the task once
    def run(self):
        
        ## The microsecond timestamp for the current iteration
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printTrace()
                # Run State 1 Code
                if self.ser.any() != 0:
                    shares.cmd = int(float(self.ser.readline()))
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
      
            else:
                pass
                
                    
            self.runs += 1
            
            # The timestamp in microseconds for the start of the next task
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    ## Updates state
    def transitionTo(self, newState):
        
        self.state = newState
     
    ## Prints a debug statement if the debug variable is True    
    def printTrace(self):
        
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time, self.start_time))
            print(str)
            
    
