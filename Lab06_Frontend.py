##@file      Lab06_Frontend.py
#@brief      This code creates a frontend UI script.
#@details    This File sends K_p values to the backend and receives collected
#            data from the backend
#@author     Brennen Irey
#@date       November 24, 2020

import serial
import matplotlib.pyplot as plt
import time

ser = serial.Serial(port = 'COM5', baudrate = 115273, timeout = 1)
omega = [] # Initializes omega array
times = [] # Initializes time array

#def FrontEnd():
    
kp = int(input('Please enter a K_p value: '))
#kp = float(kp)
#if type(kp) is int or float:
#kp = format(float(kp), '.3f')
#kp = format(kp, '.3f')
print('Collecting data, please wait.')

#kp = int(float(kp)*1000)
ser.write(str(kp).encode('ascii'))

time.sleep(12)
while True:
    #if ser.in_waiting != 0:
        if ser.readline().strip() == 0:
            pass
        else:
            line_string = ser.readline()
            line_list = line_string.strip('\n').split(',')
            omega.append(int(line_list[0]))
            times.append(int(line_list[1])/1000000) #Converts time from microseconds into seconds
    #else:
        #pass
plt.plot(time, omega)
plt.ylabel('Motor Speed, [RPM]')
plt.xlabel('Time, [s]')
plt.show()
#else:
   # print('That is not a valid value.')
    #return

ser.close()
    



