##@file      Lab03_UITask.py
#@brief      This code creates a user interface.
#@details    This file provides users with a choice of three commands to interact
#            with an encoder.
#@author     Brennen Irey
#@date       October 21, 2020

import shares
import utime
from pyb import UART

## User interface task
#
#@details This object communicates with a quadrature encoder through the serial 
#UART2 port.  The serial port waits for a character input and sends the character
#to the encoder task to carry out commands.
#
#@image html Lab03_UI_Task_FSM.png width = 1200px
class UITask:
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1 - Waiting for user input
    S1_WAIT_FOR_CHAR        = 1
    
    ## Constant defining State 2 - Waiting for encoder response
    S2_WAIT_FOR_RESP        = 2
    
    ## Constructs a user interface task
    #@param taskNum A number to identify the task
    #@param interval The time between tasks in microseconds
    #@param dbg Boolean indicating whether debug statements will print
    def __init__(self, taskNum, interval, dbg):
        
        ## The number of the task
        self.taskNum = taskNum
        
        ## The interval between tasks in microseconds
        self.interval = int(interval)
        
        ## Debug status
        self.dbg = dbg
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## A counter recording how many times the task has been run
        self.runs = 0
        
        ## The microsecond timestamp for the first run
        self.start_time = utime.ticks_us()
        
        ## The microsecond timestamp for the next iteration
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The serial port being addressed
        self.ser = UART(2)
        
    
        if self.dbg:
            print('Created user interface task')
    
    ## Runs the task once
    def run(self):
        
        ## The microsecond timestamp for the current iteration
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                print('z - Zero the encoder position \np - Print out the encoder position \nd - Print out the encoder delta')
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
                
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                self.printTrace()
                # Run State 1 Code
                if self.ser.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    shares.cmd = self.ser.readchar()
                    
            elif(self.state == self.S2_WAIT_FOR_RESP):
                self.printTrace()
                # Run State 2 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
                print('{:}'.format(shares.resp))
                shares.resp = None
                print('z - Zero the encoder position \np - Print out the encoder position \nd - Print out the encoder delta')
                    
            self.runs += 1
            
            # The timestamp in microseconds for the start of the next task
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    ## Updates state
    def transitionTo(self, newState):
        
        self.state = newState
     
    ## Prints a debug statement if the debug variable is True    
    def printTrace(self):
        
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time, self.start_time))
            print(str)
            
    
