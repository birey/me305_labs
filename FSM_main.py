##@file     FSM_main.py


from FSM_LEDs import V_LED, P_LED

## Virtual LED task - interval set to 2000 ms
task1 = V_LED(2000)

## Physical LED task
task2 = P_LED()

# To run tasks continuously
while True:
    task1.run()
    task2.run()

