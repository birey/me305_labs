##@file       Fibonacci_Generator.py
#@brief      A file that generates a Fibonacci number.
#@details    This file prompts the user for an integer input.  A check is 
#            performed to ensure the input is valid.  A Fibonacci number is 
#            then generated at the index specified by the user.
#@author     Brennen Irey
#@date       September 24, 2020


##Generates Fibonacci numbers
#
#@details This file prompts the user for an integer input.  It then checks to
# see if the input is an integer, if it fails, it checks to see if it is a 
# float.  In the event that it is a float, an error message will display, if
# the float test fails, an error message will display.  A while loop is used
# to calculate the Fibonacci sequence up to the user-specified index.
def fib ():
## idx_in is what the user inputs to the prompt
    idx_in = input('Enter a positive integer: ')    # This prompts the user 
                                                    # for an input                                            
    try:
## idx is the integer value of the user input string
        idx = int(idx_in)   # This tests the input to see if it is an integer
    except:
        try:
            idx = float(idx_in) # This tests the input to see if it is a 
                                # float.  This will only run if the integer
                                # test fails.
            print('This is not an integer, please only enter an integer.')
            # This message only displays if the user input was a float
            return
        except:
            print('This is not a number, please only enter an integer.')
            # This message only displays if the user input was a string
            return
    print ('Calculating Fibonacci number at index n = {:}:'.format(idx))
    if idx == 0:    # The first two numbers in the Fibonacci sequence cannot
                    # be calculated and must be defined.
        print('0')
    elif idx == 1:
        print('1')
    else:
## i is the while-loop counter
        i =2    # i starts at 2 because the first two indecies (0,1) are 
                # already defined
## f_n2 is f_n-2
        f_n2 = 0
## f_n1 is f_n-1
        f_n1 = 1
        while i <= idx:
## f_n is the current value of the Fibonacci sequence at index i
            f_n = f_n2 + f_n1
            f_n2 = f_n1
            f_n1 = f_n
            i += 1
        print('{:}'.format(f_n))

        repeat = input('Do you want to enter another index? (Y/N)')
        
        if repeat == 'Y':
            fib()
        else:
            return
          
        

if __name__ == '__main__':
    # Replace the following statement with the user interface code
    # that will allow testing of your Fibonnaci function. Any code
    # within the if __name__ == '__main__' block will only run when
    # the script is executed as a standalone program. If the script
    # is imported as a module the code block will not run.
    fib()