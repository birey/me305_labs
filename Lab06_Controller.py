##@file      Lab06_Controller.py
#@brief      This code creates a controller task
#@details    This file gets data from the encoder, sends data to the proportinal 
#            control function, and controls motor speed.
#@author     Brennen Irey
#@date       November 24, 2020
import shares
import utime
import pyb

## Motor controller task
#
#@details This object controls motor speed based upon commands received from 
#the User_Task object.
#
#@image html Lab06_Controller_Task_FSM.png width = 1200px
class ControllerTask:
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1 - Waiting for cmd
    S1_WAIT_FOR_CMD        = 1
    
    ## Constant defining State 2 - Collecting data
    S2_COLLECT_DATA        = 2
    
    ## Constructs a controller task
    #@param taskNum A number to identify the task
    #@param interval The time between tasks in microseconds
    #@param encoder The encoder object being used to control motor speed
    #@param motor The motor to be controlled
    #@param closedloo A method that returns a PWM percentage based on a proportional control equation
    #@param dbg Boolean indicating whether debug statements will print
    def __init__(self, taskNum, interval, encoder, motor, closedloop, dbg):
        
        ## The number of the task
        self.taskNum = taskNum
        
        ## The interval between tasks in microseconds
        self.interval = int(interval)
        
        ## Debug status
        self.dbg = dbg
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## A counter recording how many times the task has been run
        self.runs = 0
        
        ## The microsecond timestamp for the first run
        self.start_time = utime.ticks_us()
        
        ## The microsecond timestamp for the next iteration
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The elapsed time in microseconds
        self.elapsed_time = 0
        
        ## The serial port being addressed
        #self.ser = pyb.UART(2)

        ## Creates a copy of the EncoderDriver object
        self.Encoder = encoder
        
        ## Creates a copy of the MotorDriver object
        self.Motor = motor
        
        ## Creates a copy of the ClosedLoop object
        self.ClosedLoop = closedloop
        
        ## Initializes the omega array
        self.omega_arr = []
        
        ## Initializes the time array
        self.time_arr = []
        
        ## Debug code to print if dbg = True
        if self.dbg:
            print('Created controller task')
    
    ## Runs the task once
    def run(self):
        
        ## The microsecond timestamp for the current iteration
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printTrace()
                self.Encoder.update()
                
                # Run State 1 Code
                #if self.ser.any() != 0: # Used when testing in PUTTY
                if shares.cmd != None:    
                    #self.ClosedLoop.k_p = self.ser.readline() # Used when testing in PUTTY
                    self.ClosedLoop.k_p = shares.cmd 
                    self.Motor.enable() # Turns motor on
                    self.omega_arr = [] # Ensures omega array is empty
                    self.time_arr = []  # Ensures time array is empty
                    self.collection_start_time = self.curr_time # Starting time for data collection timer
                    shares.cmd = None
                    self.transitionTo(self.S2_COLLECT_DATA)
                else:
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                    
            elif(self.state == self.S2_COLLECT_DATA):
                self.printTrace()
                self.Encoder.update()
                
                # Run State 2 Code
                self.elapsed_time = utime.ticks_diff(self.curr_time, self.collection_start_time)
                self.ClosedLoop.omega = int((self.Encoder.get_delta() / self.interval)*(15000*4)) # Converts delta from ticks to RPM
                
                self.omega_arr.append(self.ClosedLoop.omega) # Adds omega value to omega array
                self.time_arr.append(self.elapsed_time)      # Adds timestamp to time array
                
                if self.elapsed_time <= 10000000: # Data collection period chosen to be 10 seconds
                    self.ClosedLoop.PropControl() # Performs proportional control calculation
                    if int(self.ClosedLoop.L) > 100:
                        self.ClosedLoop.L = 100
                    else:
                        pass
                    self.Motor.set_duty(int(self.ClosedLoop.L))
                    self.transitionTo(self.S2_COLLECT_DATA)
                else:
                    self.Motor.disable()
                    shares.omega_arr = self.omega_arr
                    shares.time_arr = self.time_arr
                    shares.resp = 1 
                    #print(self.omega_arr)
                    #print(self.time_arr)
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                    
      
            else:
                pass
                
                    
            self.runs += 1
            
            # The timestamp in microseconds for the start of the next task
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
            
            
    ## Updates state
    def transitionTo(self, newState):
        
        self.state = newState
     
    ## Prints a debug statement if the debug variable is True    
    def printTrace(self):
        
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time, self.start_time))
            print(str)