##@file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
# @mainpage
#
# @section sec_intro Introduction
#  This portfolio contains documentation for all ME 305 lab work.  All labs
#  are contained in their own page.
#
# @page page_lab01 Lab 01
#
# @section page_lab01_src Source Code Access
#
# The source code for the fibonacci program can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Fibonacci_Generator.py
#
# @section page_lab01_doc Documentation
#
# The documentation for the fibonacci program is located at @ref Fibonacci_Generator.py
#
# @page page_lab02 Lab 02
#
# @section page_lab02_src Source Code Access
#
# The source code for the FSM program can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/FSM_LEDs.py
#
# @section page_lab02_doc Documentation
#
# The documentation for the FSM program is located at @ref FSM_LEDs.py
#
# @page page_lab03 Lab 03
#
# @section page_lab03_src Source Code Access
#
# The source code for the Encoder file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab03_Encoder.py
#
# The source code for the User Interface file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab03_UITask.py
#
# @section page_lab03_doc Documentation
#
# The documentation for the Encoder file is located at @ref Lab03_Encoder.py
#
# The documentation for the User Interface file is located at @ref Lab03_UITask.py
#
# @page page_lab04 Lab 04
#
# @section page_lab04_src Source Code Access
#
# The source code for the Encoder file can be found here:
#  https://bitbucket.org/birey/me305_labs/src/master/Lab04_Encoder.py
#
# The source code for the User Interface file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab04_UITask.py
#
# The source code for the Data Collection file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab04_DataTask.py
#
# @section page_lab04_doc Documentation
#
# The documentation for the Encoder file is located at @ref Lab04_Encoder.py
#
# The documentation for the User Interface file is located at @ref Lab04_UITask.py
#
# The documentation for the Data Collection file is located at @ref Lab04_DataTask.py
#
# @page page_lab05 Lab 05
#
# @section page_lab05_src Source Code Access
#
# The source code for the UITask file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab05_UITask.py 
#
# The source code for the LEDTask file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab05_LEDTask.py
#
# The block code for the iOS app can be found here:
# https://x.thunkable.com/copy/3e9921227d48e8b5813f4d6f2e7abfcc
#
# @section page_lab05_doc Documentation
#
# The documentation for the UITask file is located at @ref Lab05_UITask.py
#
# The documentation for the LEDTask file is located at @ref Lab05_LEDTask.py
#
# @page page_lab06 Lab 06
#
# @section page_lab06_src Source Code Access
#
# The source code for the UserTask file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab06_UserTask.py
#
# The source code for the Frontend file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab06_Frontend.py
#
# The source code for the Controller file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab06_Controller.py
#
# The source code for the ClosedLoop file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab06_ClosedLoop.py
#
# @section page_lab06_doc Documentation
#
# The documentation for the UserTask file is located at @ref Lab06_UserTask.py
#
# The documentation for the Frontend file is located at @ref Lab06_Frontend.py
#
# The documentation for the Controller file is located at @ref Lab06_Controller.py
#
# The documentation for the ClosedLoop file is located at @ref Lab06_ClosedLoop.py
#
#The task diagram for this lab is shown below:
#@image html Lab06_Task_Diagram.png width = 1200px
#
# @page page_lab07 Lab 07
#
# @section page_lab07_src Source Code Access
#
# The source code for the UserTask file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab07_UserTask.py
#
# The source code for the Frontend file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab07_Frontend.py
#
# The source code for the Controller file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab07_Controller.py
#
# The source code for the ClosedLoop file can be found here:
# https://bitbucket.org/birey/me305_labs/src/master/Lab07_ClosedLoop.py
#
# @section page_lab07_doc Documentation
#
# The documentation for the UserTask file is located at @ref Lab07_UserTask.py
#
# The documentation for the Frontend file is located at @ref Lab07_Frontend.py
#
# The documentation for the Controller file is located at @ref Lab07_Controller.py
#
# The documentation for the ClosedLoop file is located at @ref Lab07_ClosedLoop.py
#
#The task diagram for this lab is shown below:
#@image html Lab07_Task_Diagram.png width = 1200px

