##@file     Lab04_main.py

from Encoder import EncoderDriver
import DataTask

## Creates an encoder object
encoder = EncoderDriver('A6', 'A7', 3, 0xFFFF, 0)

## Creates a user interface task
task0 = DataTask.DataTask(0, 1_000, encoder, 2)

# Runs tasks continuously
while True:
    task0.run()