##@file      Lab06_UserTask.py
#@brief      This code creates a backend UI task
#@details    This file reads K_p values from the frontend and sends the collected
#            data to the frontend.
#@author     Brennen Irey
#@date       November 24, 2020

import Lab06_shares as shares
import utime
import pyb 

## User task
#
#@details This object facilitates communication between a frontend located on 
# desktop/laptop, and a motor controller task located on the Nucleo.
#
#@image html Lab06_User_Task_FSM.png width = 1200px
class UserTask:
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1 - Waiting for user input
    S1_WAIT_FOR_INPUT       = 1
    
    ## Constant defining State 2 - Waiting for data
    S2_WAIT_FOR_DATA        = 2

    ## Constant defining State 3 - Sending Data
    S3_SEND_DATA            = 3
    
    ## Constructs a user interface task
    #@param taskNum A number to identify the task
    #@param interval The time between tasks in microseconds
    #@param dbg Boolean indicating whether debug statements will print
    def __init__(self, taskNum, interval, dbg):
        
        ## The number of the task
        self.taskNum = taskNum
        
        ## The interval between tasks in microseconds
        self.interval = int(interval)
        
        ## Debug status
        self.dbg = dbg
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## A counter recording how many times the task has been run
        self.runs = 0
        
        ## The microsecond timestamp for the first run
        self.start_time = utime.ticks_us()
        
        ## The microsecond timestamp for the next iteration
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The serial port being addressed
        self.ser = pyb.UART(2, baudrate = 115273)
        
        ## Index counter used for data send for loop
        self.ind = 0
        
    
        if self.dbg:
            print('Created backend task')
    
    ## Runs the task once
    def run(self):
        
        ## The microsecond timestamp for the current iteration
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_INPUT)
                
            elif(self.state == self.S1_WAIT_FOR_INPUT):
                self.printTrace()

                # Run State 1 Code
                if self.ser.any() != 0:
                    shares.cmd = float(self.ser.readline().decode('ascii')) # Decodes the serial communcation from frontend
                    #shares.cmd = int(self.ser.readline()) # Used when testing in PUTTY
                    #self.ser.write(shares.cmd)
                    self.transitionTo(self.S2_WAIT_FOR_DATA)
                else:
                    self.transitionTo(self.S1_WAIT_FOR_INPUT)
                    
            elif(self.state == self.S2_WAIT_FOR_DATA):
                self.printTrace()
                
                # Run State 2 Code
                if shares.resp == 1:
                    shares.resp = None #Clears resp value for next iteration
                    self.transitionTo(self.S3_SEND_DATA)
                else:
                    self.transitionTo(self.S2_WAIT_FOR_DATA)
                    
            elif(self.state == self.S3_SEND_DATA):
                for n in shares.omega_arr:
                    self.ser.write('{:}'.format(shares.omega_arr[self.ind]))
                    #self.ser.write(shares.omega_arr[self.ind],shares.time_arr[self.ind])
                    #print(shares.omega_arr[self.ind],shares.time_arr[self.ind])
                    #print('{:},{:}\n'.format(shares.omega_arr[self.ind], shares.time_arr[self.ind])) # Used when testing in PUTTY
                    self.ind += 1
                self.ser.write(0)
                #print(0) # Used when testing in PUTTY
                self.transitionTo(self.S1_WAIT_FOR_INPUT)
      
            else:
                pass
                
                    
            self.runs += 1
            
            # The timestamp in microseconds for the start of the next task
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    ## Updates state
    def transitionTo(self, newState):
        
        self.state = newState
     
    ## Prints a debug statement if the debug variable is True    
    def printTrace(self):
        
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time, self.start_time))
            print(str)