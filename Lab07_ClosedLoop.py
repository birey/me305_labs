##@file      Lab07_ClosedLoop.py
#@brief      This code creates a closed loop proportional controller function.
#@details    This file contains accepts the reference speed and actual speeds 
#            as inputs and returns a PWM percentage.
#@author     Brennen Irey
#@date       December 3, 2020

import pyb

## Closed loop control object
#
#@details This object calculates a dynamic PWM percentage value to be sent to 
#the motor.  It uses user-input, pre-defined terms, and hardware data to calculate
#the PWM percentage.
class ClosedLoop:
    
    ## Constructs a closed loop calculation object
    def __init__(self):
        
        ## The K_p value specified through user input
        self.k_p = 0
        
        ## The reference speed specified in the main file
        self.omega_ref = 0
        
        ## The actual speed
        self.omega = 0
        
        ## the PWM percentage to be sent to the motor
        self.L = 0
        
        ## The starting value of the performance metric, J
        self.J = 0
        
        ## The total number of runs, K
        self.K = 0
    
    ## A method which calculates a PWM percentage to be sent to the motor using
    # a proportional control equation.
    def PropControl(self, omega_ref, omega):
        
        self.L = float(self.k_p) * (float(self.omega_ref) - float(self.omega)) # Calculates L, PWM percentage
        
        return int(self.L)
    
    ## A method which calculates the performance metric, J
    def PerfMetric(self, omega_ref, omega, theta_ref, theta, k):
        
        self.J += (1/self.K)*((self.omega_ref*self.k-omega*k)^2+(theta_ref*k-theta*k)^2)
        
        return float(self.J)