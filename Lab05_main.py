##@file     Lab05_main.py

import shares
from UITask import UITask
from LEDTask import LEDTask

## Creates a user interface task
task0 = UITask(0, 1_000, False)

## Creates an encoder task
task1 = LEDTask(1, 1_000, False)

## Runs tasks sequentially
tasklist = [task0, task1]

# Runs tasks continuously
while True:
    for task in tasklist:
        task.run()