##@file      Lab07_Encoder.py
#@brief      This code creates an encoder driver object.
#@details    This file contains the EncoderDriver class.  The encoder driver 
#            configures the timer, pins, period, and prescaler.
#@author     Brennen Irey
#@date       December 3, 2020

#import shares
import utime
import pyb

## Encoder driver object
#
#@details This object sets up a timer and calculates position and change of position 
#of an encoder.
#
class EncoderDriver:
    
    ## Constructs an EncoderDriver object
    def __init__(self, pin1, pin2, timer, period, prescaler):
        
        ## The location of Pin 1
        self.pin1 = pyb.Pin(pin1)
        
        ## The location of Pin 2
        self.pin2 = pyb.Pin(pin2)
        
        ## The number of the timer to be used
        self.timer = timer
        
        ## The period of the timer
        self.period = period
        
        ## The prescaler for the timer
        self.prescaler = prescaler
        
        ## Identifies timer
        self.tim = pyb.Timer(self.timer)
        
        ## Initializes timer with prescaler and period values
        self.tim.init(prescaler = self.prescaler, period = self.period)
        
        ## Initializes timer channel 1
        self.tim.channel(1, pin = self.pin1, mode = pyb.Timer.ENC_AB)
        
        ## Initializes timer channel 2
        self.tim.channel(2, pin = self.pin2, mode = pyb.Timer.ENC_AB)
        
        self.tim.counter() 
        ## Most recent position update
        self.pos1 = 0
        
        ## Second most recent position update
        self.pos2 = 0
        
        ## Difference between two most recent position updates
        self.raw_delta = 0
        
        ## Total movement
        self.position = 0
        
        ## Difference between two most recent position updates with overflow adjustment
        self.delta = 0
    
    ## Updates the encoder position 
    def update(self):
        self.tim.counter()
        #print('C{:}'.format(self.tim.counter()))
        self.pos2 = self.pos1
        self.pos1 = self.tim.counter()
        self.raw_delta = self.pos1 - self.pos2
        if(self.raw_delta == 0):
            self.delta = 0
        elif(self.raw_delta < (-0xFFFF/2)):
            self.delta = (0xFFFF - self.pos1) + self.pos2
        elif(self.raw_delta > (0xFFFF/2)):
            self.delta = -((0xFFFF - self.pos2) + self.pos1)
        else:
            self.delta = self.raw_delta
        
        self.position += self.raw_delta/4000 # Raw_delta in ticks converted to degrees by dividing by number of ticks per revolution
        #print('pos1{:}'.format(self.pos1))
        #print('delta{:}'.format(self.delta))
    
    ## Returns the current total position, accounts for timer over and underflow.  (Units = degrees)   
    def get_position(self):
        return self.position
        
    ## Sets the position to a new value.    
    def set_position(self, new_pos):
        self.pos1 = 0
        self.pos2 = 0
        self.position = new_pos
    
    ## Returns the change in position between the two most recent position.  (Units = ticks)
    def get_delta(self):
        return self.delta
        

            
    ## Updates state    
    def transitionTo(self, newState):
        self.state = newState
    
    ## Prints a debug statement if the debug variable is True    
    def printTrace(self):
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
    
    ## Executes the command received from the serial port    
    def execute(self, cmd):
        if cmd == 90 or cmd == 122:
            self.Encoder.set_position(0)
            return 0
        
        elif cmd == 80 or cmd == 112:
            return self.Encoder.get_position()
            
        elif cmd == 68 or cmd == 100:
            return self.Encoder.get_delta()
            
            
       
            
            