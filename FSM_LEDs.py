##@file      FSM_LEDs.py
#@brief      This code is meant to familiarize me with FSMs and hardware interaction.
#@details    This file creates two tasks to be run "simultaneously."  One task 
#            blinks a virtual LED and the other task ramps up the brightness of 
#            a physical LED
#@author     Brennen Irey
#@date       October 15, 2020

import pyb

## Virtual LED task
#
#@details This object controls the blinking of a virtual LED.
#
#@image html Lab02_Virt_LED_FSM.png width = 1200px
class V_LED:
    
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0
    
    ## Constant defining State 1 - LED on
    S1_ON       = 1
    
    ## Constant defining State 2 - LED off
    S2_OFF      = 2
    
    
    def __init__(self, interval):
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## The time interval between states in ms
        self.interval = interval
        
        
    ## Runs one iteration of the task      
    def run(self):
   
        if(self.state == self.S0_INIT):
            # Run State 0 code
            self.transitionTo(self.S1_ON)
            
        elif(self.state == self.S1_ON):
            # Run State 1 code
            print('LED ON')
            pyb.delay(self.interval)    # Delays state transition by the user 
                                        # defined interval in ms
            self.transitionTo(self.S2_OFF)
            
        elif(self.state == self.S2_OFF):
            # Run State 2 code
            print('LED OFF')
            pyb.delay(self.interval)    # Delays state transition by the user 
                                        # defined interval in ms
            self.transitionTo(self.S1_ON)
            
            
    ## Updates the variable defining the next state to run
    def transitionTo(self, newState):
      
        self.state = newState
        

## Physical LED task
#
#@details This object controls the intensity of a physical LED.
#
#@image html Lab02_Phys_LED_FSM.png width = 1200px
class P_LED:
     
    ## Constant defining State 0 - Initialization
    S0_INIT         = 0
    
    ## Constant defining State 1 - Increasing PWM
    S1_INCREASING   = 1
    
    ## Constant defining State 2 - Resetting PWM to zero
    S2_RESET        = 2
    
    
    def __init__(self):
        
        ## The state to run on the next iteration of the task.
        self.state      = self.S0_INIT
        
        ## The current voltage represented as pulse width percent
        self.V_CURR     = 0
        
        ## The maximum voltage represented as 100 percent pulse width
        self.V_MAX      = 100
        
        ## The pin for the user LED
        self.pinA5      = pyb.Pin(pyb.Pin.cpu.A5)
        
        ## The timer and frequency being used
        self.tim2       = pyb.Timer(2, freq = 20000)
        
        ## The timer cahnnel being used
        self.t2ch1      = self.tim2.channel(1, pyb.Timer.PWM, pin = self.pinA5)
        
          
    ## Runs one iteration of the task    
    def run(self):
    
        if(self.state == self.S0_INIT):
            # Run State 0 code
            self.t2ch1.pulse_width_percent(self.V_CURR)
            pyb.delay(500)
            self.transitionTo(self.S1_INCREASING)
            
        elif(self.state == self.S1_INCREASING):
            #Run state 1 code
            # Checking if the current voltage is less than the maximum voltage
            if(self.V_CURR < self.V_MAX):
                self.V_CURR += 5    # Increases pulse width by 5 percent
                self.t2ch1.pulse_width_percent(self.V_CURR)
                pyb.delay(500)      # Delays state transition by 500 ms
                self.transitionTo(self.S1_INCREASING)
            else:
                # Runs when V_CURR = V_MAX
                pyb.delay(500)      # Delays state transition by 500 ms
                self.transitionTo(self.S2_RESET)
        elif(self.state == self.S2_RESET):
            self.V_CURR = 0     # Resets pulse width to 0
            self.t2ch1.pulse_width_percent(self.V_CURR)
            pyb.delay(500)      # Delays state transition by 500 ms
            self.transitionTo(self.S1_INCREASING)
                
             
    ## Updates the variable defining the next state to run        
    def transitionTo(self, newState):
        
        self.state = newState