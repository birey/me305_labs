##@file      Lab04_UITask.py
#@brief      This code creates a user interface.
#@details    This file provides users with a choice of two commands to control
#            data collection.
#@author     Brennen Irey
#@date       November 4, 2020

import serial
import keyboard
import time
import matplotlib.pyplot as plt


class UITask:
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1 - Begin collection
    S1_BEGIN_COLLECTION     = 1
    
    ## Constant defining State 2 - Collection
    S2_COLLECTION           = 2
    
    ## Constant defining State 3 - Plotting
    S3_PLOTTING             = 3
    
    ## Constructs a user interface task
    #
    def __init__(self, taskNum, interval, port):
        
        ## The number of the task
        self.taskNum = taskNum
        
        ## The interval between tasks in microseconds
        self.interval = interval
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## A counter recording how many times the task has been run
        self.runs = 0
        
        ## The seconds timestamp for the first run
        self.start_time = time.time()
        
        ## The seconds timestamp for the current time
        self.curr_time = 0
        
        ## The seconds timestamp for the next iteration
        self.next_time = self.start_time + self.interval
        
         ## The seconds timestamp for time elapsed
        self.time_elapsed = self.curr_time - self.start_time
        
        ## Assigns the serial port
        self.ser = serial.Serial(port = port, baudrate = 115273, timeout = 1)
        
        ## A command variable
        self.cmd = 0
        
        ## A line of text
        self.line_string = 0
        
        ## A separated line of text
        self.line_list = []
        
        ## Timestamp list
        self.time = []
        
        ## Position list
        self.position = []
        
    ## Runs the task once
    def run(self):
        
        ## The microsecond timestamp for the current iteration
        self.curr_time = time.time()
        if((self.curr_time - self.next_time) >= 0):
            
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_BEGIN_COLLECTION)
            
            elif(self.state == self.S1_BEGIN_COLLECTION):
                #self.cmd = input(' Press "G" to begin data collection.')
                print(' Press "G" to begin data collection.')
                #if(self.cmd == 'g' or self.cmd == 'G'):
                if(keyboard.is_pressed('g' or 'G')):
                    #self.ser.write(str(self.cmd).encode('ascii'))
                    self.ser.write('g'.encode('ascii'))
                    self.cmd = 0
                    self.transitionTo(self.S2_COLLECTION)
                #else:
                 #   print("That is not a valid command.")
                  #  self.transitionTo(self.S1_BEGIN_COLLECTION)
            
            elif(self.state == self.S2_COLLECTION):
                if(keyboard.is_pressed('s' or 'S')):
                    self.ser.write('s'.encode('ascii'))
                    self.transitionTo(self.S3_PLOTTING)
                #elif(self.time_elapsed >= 10):
                    self.transitionTo(self.S3_PLOTTING)
                else:
                    self.transitionTo(self.S2_COLLECTION)
            
            elif(self.state == self.S3_PLOTTING):
                print('Beginning plotting.')
                #self.line_string = self.ser.readline()
                #print('{:}'.format(self.line_string))
                self.line_list = self.ser.readline().decode('ascii').strip('\n').split(',')
                if(self.line_list[0] == 'end'):
                    plt.plot(self.time,self.position)
                    plt.xlabel('Time (us)')
                    plt.ylabel('Position (Ticks)')
                    plt.show()
                    self.transitionTo(self.S1_BEGIN_COLLECTION)
                else:
                    #self.line_list = self.ser.readline().decode('ascii').strip('\n').split(',')
                    self.time.append(int(self.line_list[0]))
                    self.position.append(int(self.line_list[1]))
                    
            self.runs += 1
            
            self.next_time = self.next_time + self.interval
            
            self.time_elapsed = self.curr_time - self.start_time
            
            
    ## Updates state    
    def transitionTo(self, newState):
        self.state = newState