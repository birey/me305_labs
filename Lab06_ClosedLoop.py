##@file      Lab06_ClosedLoop.py
#@brief      This code creates a closed loop proportional controller function.
#@details    This file contains accepts the reference speed and actual speeds 
#            as inputs and returns a PWM percentage.
#@author     Brennen Irey
#@date       November 24, 2020

import pyb

## Closed loop control object
#
#@details This object calculates a dynamic PWM percentage value to be sent to 
#the motor.  It uses user-input, pre-defined terms, and hardware data to calculate
#the PWM percentage.
class ClosedLoop:
    
    ## Constructs a closed loop calculation object
    #@param omega_ref The reference speed specified by the user 
    #@param omega The actual speed received from encoder data
    def __init__(self, omega_ref, omega):
        
        ## The K_p value specified through user input
        self.k_p = 0
        
        ## The reference speed specified in the main file
        self.omega_ref = (omega_ref)
        
        ## The actual speed
        self.omega = (omega)
        
        ## the PWM percentage to be sent to the motor
        self.L = 0
    
    ## A method which calculates a PWM percentage to be sent to the motor using
    # a proportional control equation.
    def PropControl(self):
        
        self.L = float(self.k_p) * (float(self.omega_ref) - float(self.omega)) # Calculates L, PWM percentage
        
        return int(self.L)