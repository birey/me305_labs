##@file     Lab03_main.py

import shares
from UITask import UITask
from Encoder import EncoderDriver, EncoderTask 

## Creates an encoder object
encoder = EncoderDriver('A6', 'A7', 3, 0xFFFF, 0)

## Creates a user interface task
task0 = UITask(0, 1_000, False)

## Creates an encoder task
task1 = EncoderTask(1, 10_000, encoder, False)

## Runs tasks sequentially
tasklist = [task0, task1]

# Runs tasks continuously
while True:
    for task in tasklist:
        task.run()